package ru.terrakok.gitlabclient.ui.mergerequest

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_mr_commits.*
import ru.terrakok.gitlabclient.R
import ru.terrakok.gitlabclient.entity.app.CommitWithShortUser
import ru.terrakok.gitlabclient.extension.showSnackMessage
import ru.terrakok.gitlabclient.presentation.global.Paginator
import ru.terrakok.gitlabclient.presentation.mergerequest.commits.MergeRequestCommitsPresenter
import ru.terrakok.gitlabclient.presentation.mergerequest.commits.MergeRequestCommitsView
import ru.terrakok.gitlabclient.ui.global.BaseFragment
import ru.terrakok.gitlabclient.ui.global.list.CommitAdapterDelegate
import ru.terrakok.gitlabclient.ui.global.list.isSame

/**
 * Created by Eugene Shapovalov (@CraggyHaggy) on 20.10.18.
 */
class MergeRequestCommitsFragment : BaseFragment(), MergeRequestCommitsView {

    override val layoutRes = R.layout.fragment_mr_commits

    @InjectPresenter
    lateinit var presenter: MergeRequestCommitsPresenter

    @ProvidePresenter
    fun providePresenter() =
        scope.getInstance(MergeRequestCommitsPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        paginalRenderView.init(
            { presenter.refreshCommits() },
            { presenter.loadNextCommitsPage() },
            { o, n ->
                if (o is CommitWithShortUser && n is CommitWithShortUser) {
                    o.isSame(n)
                } else false
            },
            CommitAdapterDelegate()
        )
    }

    override fun renderPaginatorState(state: Paginator.State) {
        paginalRenderView.render(state)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }
}